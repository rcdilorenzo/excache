defmodule ExCache do
  use ExActor.GenServer, export: :ex_cache

  definit time do
    initial_state(%{expire_time: time || 300, cache: HashDict.new, cache_timings: HashDict.new})
  end

  defcall expire_time, state: state, do: reply(state.expire_time)

  defcast config(expire_time: time), state: state do
    state |> Map.put(:expire_time, time) |> new_state
  end

  defcall execute(func, key), state: state do
    if !HashDict.has_key?(state.cache, key) or is_stale(key, state.cache_timings[key], state) do
      update_cache_value(func, key, state)
    else
      get_cache_value(func, key, state)
    end
  end

  defcast clear, state: state do
    %{state | cache: HashDict.new, cache_timings: HashDict.new} |> new_state
  end

  # Private funtions

  defp get_cache_value(func, key, state) do
    reply(state.cache[key])
  end

  defp update_cache_value(func, key, state) do
    value = func.()
    new_cache_state = HashDict.put(state.cache, key, value)
    new_cache_timings = HashDict.put(state.cache_timings, key, now_in_seconds)
    new_state = Map.put(state, :cache, new_cache_state)
                  |> Map.put(:cache_timings, new_cache_timings)
    set_and_reply(new_state, value)
  end

  defp is_stale(key, last_updated_time, state) do
    (now_in_seconds - last_updated_time) >= state.expire_time
  end

  defp now_in_seconds do
    {megaseconds, seconds, _} = :erlang.now
    (1_000_000 * megaseconds) + seconds
  end
end

