# ExCache

![Tests](https://travis-ci.org/rcdilorenzo/ExCache.svg)

# Installation

Add the following as a dependency in your mix.exs file:
```elixir
{ :excache, github: "rcdilorenzo/excache" }
```

# Usage

Before you start using ExCache, you need to start it:
```elixir
ExCache.start(expire_time) # staleness defaults to 60 seconds
```

To use simply call ExCache with a function and a key. For example:
```elixir
ExCache.execute(fn -> String.capitalize("rcdilorenzo") end, :cap_rcd)
```

The cache can be cleared by simply calling:
```elixir
ExCache.clear
```

You can also configure the timeout value for the cache:
```elixir
ExCache.config(expire_time: 5 * 60)
```
