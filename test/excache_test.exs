defmodule ExCacherTest do
  use ExUnit.Case, async: false
  ExCache.start

  setup do
    ExCache.config(expire_time: 60)
    ExCache.clear
    :ok
  end

  test "cache can be configured" do
    assert ExCache.expire_time == 60
    ExCache.config(expire_time: 1)
    assert ExCache.expire_time == 1
  end

  test "executes a given function" do
    assert ExCache.execute(fn -> 1 end, :number_func) == 1
  end

  test "doesn't execute function if cache is not stale" do
    assert ExCache.execute(fn -> 1 end, :number_func) == 1
    assert ExCache.execute(fn -> 2 end, :number_func) == 1
  end

  test "executes function if cache is stale" do
    assert ExCache.execute(fn -> 1 end, :number_func) == 1
    ExCache.config expire_time: 0
    assert ExCache.execute(fn -> 2 end, :number_func) == 2
  end

end

